const map = [
   "WWWWWWWWWWWWWWWWWWWWW",
   "W   W     W     W W W",
   "W W W WWW WWWWW W W W",
   "W W W   W     W W   W",
   "W WWWWWWW W WWW W W W",
   "W         W     W W W",
   "W WWW WWWWW WWWWW W W",
   "W W   W   W W     W W",
   "W WWWWW W W W WWW W F",
   "S     W W W W W W WWW",
   "WWWWW W W W W W W W W",
   "W     W W W   W W W W",
   "W WWWWWWW WWWWW W W W",
   "W       W       W   W",
   "WWWWWWWWWWWWWWWWWWWWW",
];
let posicaoLinha = 9;
let posicaoColuna = 0; 

const gerarLinhaDiv = () => {
   const divpai = document.querySelector(".labirinto");

   let criarDiv = document.createElement("div");
   criarDiv.setAttribute("class", "linha")
   
   divpai.appendChild(criarDiv);

   return criarDiv;
}

const gerarCelulaJogador = (linhaPai, numLinha) => {

   let criarDiv = document.createElement("div");
   criarDiv.setAttribute("class", "celulaJogador");
   criarDiv.setAttribute("id", `${numLinha}`);
   
    
   linhaPai.appendChild(criarDiv)
   
   return criarDiv;
}

const gerarCelulaFim = (linhaPai, numLinha) => {

   let criarDiv = document.createElement("div");
   criarDiv.setAttribute("class", "celulaFim");
   criarDiv.setAttribute("id", `${numLinha}`);
   
    
   linhaPai.appendChild(criarDiv)
   
   return criarDiv;
}


const gerarCelulaParede = (linhaPai) => {

   let criarDiv = document.createElement("div");
   criarDiv.setAttribute("class", "celulaParede")
   
   
   linhaPai.appendChild(criarDiv)
   
   return criarDiv;
}

const gerarCelula = (linhaPai, numLinha, numColuna) => {

   let criarDiv = document.createElement("div");
   criarDiv.setAttribute("class", "celula")
   criarDiv.setAttribute("id", `${numLinha}-${numColuna}`);

   
   linhaPai.appendChild(criarDiv)
   
   return criarDiv;
}

for(let i = 0; i < map.length; i++){
   
  let linhaCriada = gerarLinhaDiv();


   for(let j = 0; j < map[i].length; j++){
      
      if(map[i][j] === "W"){
         gerarCelulaParede(linhaCriada);
      }

      else if(map[i][j] === "F"){
         gerarCelulaFim(linhaCriada,"fim") ;
      }

      else if (map[i][j] === "S") {

   let criarDiv = document.createElement("div");
   criarDiv.setAttribute("class", "celula")
   criarDiv.setAttribute("id", `${[i]}-${[j]}`);
   criarDiv.innerHTML = " ";

   linhaCriada.appendChild(criarDiv)
   criarDiv.appendChild(gerarCelulaJogador(linhaCriada,"jogador"));
      }


      else{
         gerarCelula(linhaCriada, i, j);
      }
   }
}




const movimentos = (evt) => {

   const currentKey = evt.key;
   

   if(currentKey === "ArrowRight"){
      posicaoColuna += 1

      let player = document.getElementById('jogador');
      let celula2 = document.getElementById(`${posicaoLinha}-${posicaoColuna}`);

      player.setAttribute("class", "slideLeft");
      setTimeout( () => { player.classList.remove("slideLeft"); }, 100);

      if(celula2 === null){
         posicaoColuna -=1
      }else{
         celula2.appendChild(player)
      }
      
   }

   else if(currentKey === "ArrowLeft"){
      posicaoColuna -= 1

      let player = document.getElementById('jogador');
      let celula2 = document.getElementById(`${posicaoLinha}-${posicaoColuna}`);
      
      player.setAttribute("class", "slideRight");
      setTimeout( () => { player.classList.remove("slideRight"); }, 100);

      if(celula2 === null){
         posicaoColuna +=1
      }else{
         celula2.appendChild(player)
      }
      


   }

   else if(currentKey === "ArrowUp"){
      posicaoLinha -= 1

      let player = document.getElementById('jogador');
      let celula2 = document.getElementById(`${posicaoLinha}-${posicaoColuna}`);
      

     

      if(celula2 === null){
         posicaoLinha +=1
      }else{
         celula2.appendChild(player)
      }
      
   }

   else if(currentKey === "ArrowDown"){
      posicaoLinha += 1

      let player = document.getElementById('jogador');
      let celula2 = document.getElementById(`${posicaoLinha}-${posicaoColuna}`);
      
      player.setAttribute("class", "slideDown");
      setTimeout( () => { player.classList.remove("slideDown"); }, 10000);
      if(celula2 === null){
         posicaoLinha -=1
      }else{
         celula2.appendChild(player)
      }
      
   }

   if(posicaoColuna === 19 && posicaoLinha === 8){
      
      removeEventListener("keydown", movimentos)
      
      let labirinto = document.getElementById("labirinto");
      labirinto.style ="display: none;"

      vitoria();
   }

};
addEventListener("keydown", movimentos)


const vitoria = () => {
      const vitoria = document.querySelector(".vitoria");
      const body = document.querySelector("body");

      vitoria.style = "display: flex;"
      body.style= "background-image: none; background-color: white;"
}

const reset = () => {
   const vitoria = document.querySelector(".vitoria");
   vitoria.style = "display: none;"

      
   let labirinto = document.getElementById("labirinto");
   labirinto.style ="display: block;"

   posicaoLinha = 9;
   posicaoColuna = 0;

   let player = document.getElementById('jogador');
   let celula2 = document.getElementById(`${posicaoLinha}-${posicaoColuna}`);

   celula2.appendChild(player)

   const body = document.querySelector("body");
   
   body.style= "background-image:url(../images/floor.png);"

      addEventListener("keydown", movimentos)
}

const resetBtn = document.getElementById("reset")

resetBtn.addEventListener("click", reset);
